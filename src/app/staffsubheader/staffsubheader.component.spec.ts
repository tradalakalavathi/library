import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffsubheaderComponent } from './staffsubheader.component';

describe('StaffsubheaderComponent', () => {
  let component: StaffsubheaderComponent;
  let fixture: ComponentFixture<StaffsubheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffsubheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffsubheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
