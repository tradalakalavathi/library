import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpachomeComponent } from './opachome.component';

describe('OpachomeComponent', () => {
  let component: OpachomeComponent;
  let fixture: ComponentFixture<OpachomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpachomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpachomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
