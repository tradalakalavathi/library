import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-clientbookcard',
  templateUrl: './clientbookcard.component.html',
  styleUrls: ['./clientbookcard.component.scss']
})
export class ClientbookcardComponent implements OnInit {

  @Input()  bookTitle: string;
  @Input() author: string;
  @Input() bookCategory: string;
  @Input() bookImageUrl: string;

  constructor() { }

  ngOnInit(): void {
  }

}
