import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientbookcardComponent } from './clientbookcard.component';

describe('ClientbookcardComponent', () => {
  let component: ClientbookcardComponent;
  let fixture: ComponentFixture<ClientbookcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientbookcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientbookcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
