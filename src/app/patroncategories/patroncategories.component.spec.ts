import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatroncategoriesComponent } from './patroncategories.component';

describe('PatroncategoriesComponent', () => {
  let component: PatroncategoriesComponent;
  let fixture: ComponentFixture<PatroncategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatroncategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatroncategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
