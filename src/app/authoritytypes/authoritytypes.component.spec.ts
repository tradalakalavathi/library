import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthoritytypesComponent } from './authoritytypes.component';

describe('AuthoritytypesComponent', () => {
  let component: AuthoritytypesComponent;
  let fixture: ComponentFixture<AuthoritytypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthoritytypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthoritytypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
