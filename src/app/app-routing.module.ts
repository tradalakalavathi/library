import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { OpachomeComponent } from './opachome/opachome.component';
import { PatroncategoriesComponent } from './patroncategories/patroncategories.component';
import { AuthoritytypesComponent } from './authoritytypes/authoritytypes.component';
import { MarcframeworkComponent } from './marcframework/marcframework.component';
import { StaffdashboardComponent } from './staffdashboard/staffdashboard.component';
import { ClientdashboardComponent } from './clientdashboard/clientdashboard.component';
import { AuthService } from './auth.service';


const routes: Routes = [
  {path: 'admindashboard', component: AdmindashboardComponent,canActivate: [AuthService]},
  {path: '', component: OpachomeComponent},
  {path: 'staffdashboard', component: StaffdashboardComponent,canActivate: [AuthService]},
  {path: 'patroncategories', component: PatroncategoriesComponent,canActivate: [AuthService]},
  {path: 'authoritytypes', component: AuthoritytypesComponent,canActivate: [AuthService]},
  {path: 'marcframework', component: MarcframeworkComponent,canActivate: [AuthService]},
  {path: 'clientdashboard', component: ClientdashboardComponent,canActivate: [AuthService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthService]
})
export class AppRoutingModule { }
