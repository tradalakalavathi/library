import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientdashboardheaderComponent } from './clientdashboardheader.component';

describe('ClientdashboardheaderComponent', () => {
  let component: ClientdashboardheaderComponent;
  let fixture: ComponentFixture<ClientdashboardheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientdashboardheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientdashboardheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
