import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpacheadersearchComponent } from './opacheadersearch.component';

describe('OpacheadersearchComponent', () => {
  let component: OpacheadersearchComponent;
  let fixture: ComponentFixture<OpacheadersearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpacheadersearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpacheadersearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
