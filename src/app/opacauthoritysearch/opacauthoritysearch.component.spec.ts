import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpacauthoritysearchComponent } from './opacauthoritysearch.component';

describe('OpacauthoritysearchComponent', () => {
  let component: OpacauthoritysearchComponent;
  let fixture: ComponentFixture<OpacauthoritysearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpacauthoritysearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpacauthoritysearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
