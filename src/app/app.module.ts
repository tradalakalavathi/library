import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { AdminheaderComponent } from './adminheader/adminheader.component';
import { AdminsidebarComponent } from './adminsidebar/adminsidebar.component';
import { OpacheaderComponent } from './opacheader/opacheader.component';
import { OpachomeComponent } from './opachome/opachome.component';
import { OpacsidebarComponent } from './opacsidebar/opacsidebar.component';
import { OpacloginComponent } from './opaclogin/opaclogin.component';
import { OpacauthoritysearchComponent } from './opacauthoritysearch/opacauthoritysearch.component';
import { OpacheadertopbarComponent } from './opacheadertopbar/opacheadertopbar.component';
import { OpacheadersearchComponent } from './opacheadersearch/opacheadersearch.component';
import { OpacfooterComponent } from './opacfooter/opacfooter.component';
import { RecentcommentscardComponent } from './recentcommentscard/recentcommentscard.component';
import { PatroncategoriesComponent } from './patroncategories/patroncategories.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { MatIconModule } from '@angular/material/icon';
import { AuthoritytypesComponent } from './authoritytypes/authoritytypes.component';
import { MarcframeworkComponent } from './marcframework/marcframework.component';
import { AdminsubheaderComponent } from './adminsubheader/adminsubheader.component';
import { StaffsubheaderComponent } from './staffsubheader/staffsubheader.component';
import { StaffdashboardComponent } from './staffdashboard/staffdashboard.component';
import { StaffsidebarComponent } from './staffsidebar/staffsidebar.component';
import { ClientdashboardComponent } from './clientdashboard/clientdashboard.component';
import { ToastrModule } from 'ngx-toastr';
import { ClientdashboardheaderComponent } from './clientdashboardheader/clientdashboardheader.component';
import { FormsModule } from '@angular/forms';
import { ClientdashboardsidebarComponent } from './clientdashboardsidebar/clientdashboardsidebar.component';
import { ClientdashboardmainComponent } from './clientdashboardmain/clientdashboardmain.component';
import { ClientbookscarouselComponent } from './clientbookscarousel/clientbookscarousel.component';
import { ClientbookcardComponent } from './clientbookcard/clientbookcard.component';
import { HttpClientModule } from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader'; // <============


// import { MatTableModule } from '@angular/material/table';
// import { MatPaginatorModule } from '@angular/material/paginator';
// import { MatSortModule } from '@angular/material/sort';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdmindashboardComponent,
    AdminheaderComponent,
    AdminsidebarComponent,
    OpacheaderComponent,
    OpachomeComponent,
    OpacsidebarComponent,
    OpacloginComponent,
    OpacauthoritysearchComponent,
    OpacheadertopbarComponent,
    OpacheadersearchComponent,
    OpacfooterComponent,
    RecentcommentscardComponent,
    PatroncategoriesComponent,
    AuthoritytypesComponent,
    MarcframeworkComponent,
    AdminsubheaderComponent,
    StaffsubheaderComponent,
    StaffdashboardComponent,
    StaffsidebarComponent,
    ClientdashboardComponent,
    ClientdashboardheaderComponent,
    ClientdashboardsidebarComponent,
    ClientdashboardmainComponent,
    ClientbookscarouselComponent,
    ClientbookcardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    MatIconModule,
    FormsModule,
    HttpClientModule,
    NgHttpLoaderModule.forRoot(),
    ToastrModule.forRoot() // ToastrModule added

    // MatTableModule,
    // MatPaginatorModule,
    // MatSortModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
