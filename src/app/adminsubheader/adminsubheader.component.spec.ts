import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminsubheaderComponent } from './adminsubheader.component';

describe('AdminsubheaderComponent', () => {
  let component: AdminsubheaderComponent;
  let fixture: ComponentFixture<AdminsubheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminsubheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminsubheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
