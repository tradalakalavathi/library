import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpacheaderComponent } from './opacheader.component';

describe('OpacheaderComponent', () => {
  let component: OpacheaderComponent;
  let fixture: ComponentFixture<OpacheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpacheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpacheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
