import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentcommentscardComponent } from './recentcommentscard.component';

describe('RecentcommentscardComponent', () => {
  let component: RecentcommentscardComponent;
  let fixture: ComponentFixture<RecentcommentscardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentcommentscardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentcommentscardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
