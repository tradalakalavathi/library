import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http:HttpClient) { }
   url:any = "http://18.222.96.130:3500/login"
  loginService(data){
    let option = new HttpHeaders({ 'Content-Type': 'application/json'});
    return new Promise((resolve, reject) => {
     this.http.post(this.url,data,{headers:option}).subscribe((res)=>{resolve(res)},(err)=>{reject(err)})
    })
  }
}
