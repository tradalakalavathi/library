import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientdashboardsidebarComponent } from './clientdashboardsidebar.component';

describe('ClientdashboardsidebarComponent', () => {
  let component: ClientdashboardsidebarComponent;
  let fixture: ComponentFixture<ClientdashboardsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientdashboardsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientdashboardsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
