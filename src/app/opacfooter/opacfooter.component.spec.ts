import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpacfooterComponent } from './opacfooter.component';

describe('OpacfooterComponent', () => {
  let component: OpacfooterComponent;
  let fixture: ComponentFixture<OpacfooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpacfooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpacfooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
