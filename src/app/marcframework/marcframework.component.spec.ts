import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcframeworkComponent } from './marcframework.component';

describe('MarcframeworkComponent', () => {
  let component: MarcframeworkComponent;
  let fixture: ComponentFixture<MarcframeworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcframeworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcframeworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
