import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientbookscarouselComponent } from './clientbookscarousel.component';

describe('ClientbookscarouselComponent', () => {
  let component: ClientbookscarouselComponent;
  let fixture: ComponentFixture<ClientbookscarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientbookscarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientbookscarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
