import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-clientbookscarousel',
  templateUrl: './clientbookscarousel.component.html',
  styleUrls: ['./clientbookscarousel.component.scss']
})
export class ClientbookscarouselComponent implements OnInit {

  @Input() carouselTitle: string;

  constructor() { }

  ngOnInit(): void {
  }

}
