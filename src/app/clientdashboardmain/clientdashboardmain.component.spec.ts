import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientdashboardmainComponent } from './clientdashboardmain.component';

describe('ClientdashboardmainComponent', () => {
  let component: ClientdashboardmainComponent;
  let fixture: ComponentFixture<ClientdashboardmainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientdashboardmainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientdashboardmainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
