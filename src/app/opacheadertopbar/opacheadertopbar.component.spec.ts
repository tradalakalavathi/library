import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpacheadertopbarComponent } from './opacheadertopbar.component';

describe('OpacheadertopbarComponent', () => {
  let component: OpacheadertopbarComponent;
  let fixture: ComponentFixture<OpacheadertopbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpacheadertopbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpacheadertopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
