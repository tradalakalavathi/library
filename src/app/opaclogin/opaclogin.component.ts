import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service'
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router'


@Component({
  selector: 'app-opaclogin',
  templateUrl: './opaclogin.component.html',
  styleUrls: ['./opaclogin.component.scss']
})
export class OpacloginComponent implements OnInit {
  regData: any = { email: "", password: "" }
  constructor(private app: AppService, private toaster: ToastrService, private route: Router) { }

  ngOnInit(): void {
    localStorage.removeItem('isLogin')
  }
  sumbit() {
    if (this.regData.email == "" || this.regData.email == undefined) {
      this.toaster.error("Please enter username", "Input Missing")
    } else if (this.regData.password == "" || this.regData.password == undefined) {
      this.toaster.error("Please enter password", "Input Missing")
    } else {
      this.app.loginService(this.regData).then((res) => {
        if (res['error']) {
          this.toaster.error(res['message'], "Error")
        } else {
          var dataValue = res['data']
          localStorage.setItem("isLogin","true")
          this.toaster.success("Login Successfully","Success")
          if (dataValue['type'] == 0) {
            this.route.navigate(['admindashboard'])
          } else if (dataValue['type'] == 1) {
            this.route.navigate(['staffdashboard'])
          } else if (dataValue['type'] == 2) {
            this.route.navigate(['clientdashboard'])
          }
        }

      })
    }


  }

}
