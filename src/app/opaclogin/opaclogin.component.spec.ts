import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpacloginComponent } from './opaclogin.component';

describe('OpacloginComponent', () => {
  let component: OpacloginComponent;
  let fixture: ComponentFixture<OpacloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpacloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpacloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
