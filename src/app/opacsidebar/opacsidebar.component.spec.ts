import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpacsidebarComponent } from './opacsidebar.component';

describe('OpacsidebarComponent', () => {
  let component: OpacsidebarComponent;
  let fixture: ComponentFixture<OpacsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpacsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpacsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
